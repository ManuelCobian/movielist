package com.example.movielist.api


import com.example.movielist.models.Response.TopResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("top_rated")
    fun get_top_movies(@Query("api_key")api_key:String
                       ,@Query("language")leng:String
                       ,@Query("page") page:Int):Observable<TopResponse>


    @GET("/3/movie/popular")
    fun get_popular_movies(@Query("api_key") key: String):Observable<TopResponse>



    @GET("/3/movie/popular")
    fun get_movie(@Query("movie_id")id:Int, @Query("api_key") key: String):Observable<TopResponse>


}