package com.example.movielist.dl

import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.movielist.api.Api
import com.example.movielist.app.ServiceBuilder
import com.example.movielist.models.Response.ResultResponse
import com.example.movielist.models.Response.TopResponse
import com.example.movielist.ul.adapters.MoviesAdapter
import com.example.movielist.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home.*

class MovieModel: ViewModel() {
    lateinit var apiWeb: Api
    val constants= Constants();
    val compositeDisposable = CompositeDisposable()

    private val moviesTop: MutableLiveData<List<ResultResponse>> by lazy {
        MutableLiveData<List<ResultResponse>>().also {
            loadMoviesTop()
        }
    }


    private val moviesFavorites: MutableLiveData<List<ResultResponse>> by lazy {
        MutableLiveData<List<ResultResponse>>().also {
            loadMoviesFavorites()
        }
    }


    private val moviesList: MutableLiveData<List<ResultResponse>> by lazy {
        MutableLiveData<List<ResultResponse>>().also {
            loadMoviesList()
        }
    }



    fun getTopMovie(): LiveData<List<ResultResponse>> {
        return moviesTop
    }

    fun getTFavoritesMovie(): LiveData<List<ResultResponse>> {
        return moviesFavorites
    }

    fun getTListMovie(): LiveData<List<ResultResponse>> {
        return moviesList
    }

    public fun loadMoviesTop() {
        ServiceBuilder.buildService().get_popular_movies(constants.Api_web)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({response -> onResponse(response)}, {t -> onFailure(t) })
    }


    public fun loadMoviesFavorites() {
        ServiceBuilder.buildService().get_top_movies(constants.Api_web, "en-US", 1)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({response -> onResponseList(response)}, {t -> onFailureList(t) })
    }




    public fun loadMoviesList() {
        ServiceBuilder.buildService().get_top_movies(constants.Api_web, "en-US", 2)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({response -> onResponseFavorites(response)}, {t -> onFailureFavorites(t) })
    }

    private fun onFailureFavorites(t: Throwable?) {

    }

    private fun onResponseFavorites(response: TopResponse?) {
        moviesFavorites.value=response!!.results
    }

    private fun onFailure(t: Throwable?) {
        //Toast.makeText(getActivity(),"Error", Toast.LENGTH_SHORT).show()
    }

    private fun onResponse(response: TopResponse?) {
        //Toast.makeText(getActivity(),response!!.page.toString(), Toast.LENGTH_SHORT).show()

       moviesTop.value=response!!.results

    }

    private fun onFailureList(t: Throwable?) {

    }

    private fun onResponseList(response: TopResponse?) {
        moviesList.value=response!!.results
    }


}