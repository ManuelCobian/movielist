package com.example.movielist.dl

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MovieViewModelFactory(): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(MovieModel::class.java)){
            return MovieModel() as T
        }
        throw IllegalArgumentException ("UnknownViewModel")
    }

}