package com.example.movielist.app

import com.example.movielist.api.Api
import com.example.movielist.utils.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.URL
import java.util.concurrent.TimeUnit




object ServiceBuilder {

    val URL_BASE=Constants();
    private val client = OkHttpClient.Builder()
        .connectTimeout(100, TimeUnit.SECONDS)
        .readTimeout(100,TimeUnit.SECONDS).build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(URL_BASE.BASE_URL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()
        .create(Api::class.java)

    fun buildService(): Api {
        return retrofit
    }
}