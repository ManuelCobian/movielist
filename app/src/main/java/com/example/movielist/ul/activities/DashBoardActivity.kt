package com.example.movielist.ul.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.movielist.R
import androidx.databinding.DataBindingUtil
import com.example.movielist.app.App
import com.example.movielist.app.ServiceBuilder
import com.example.movielist.databinding.ActivityDashBoardBinding
import com.example.movielist.models.Response.TopResponse
import com.example.movielist.ul.fragments.FavoritosFragment
import com.example.movielist.ul.fragments.HomeFragment
import com.example.movielist.ul.fragments.TopFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class DashBoardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        val databiding=ActivityDashBoardBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(databiding.root)
        //getSupportActionBar()?.hide()
        openHomeFragment();

        databiding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.home -> {
                   // Toast.makeText(this,"Menu home",Toast.LENGTH_SHORT).show()
                    val newFragment = HomeFragment()
                    val transaction = supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.content_frame, newFragment)
                    transaction.addToBackStack(null)
                    transaction.commit()
                }
                R.id.terror -> {
                    //Toast.makeText(this,"Menu Terror",Toast.LENGTH_SHORT).show()
                    //Toast.makeText(this,"favoritos",Toast.LENGTH_SHORT).show()

                    val newFragment = TopFragment()
                    val transaction = supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.content_frame, newFragment)
                    transaction.addToBackStack(null)
                    transaction.commit()
                }
                R.id.favoritos -> {
                    //Toast.makeText(this,"favoritos",Toast.LENGTH_SHORT).show()
                    val newFragment = FavoritosFragment()
                    val transaction = supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.content_frame, newFragment)
                    transaction.addToBackStack(null)
                    transaction.commit()
                }
            }
            true
        }

    }



    private fun openHomeFragment(){
        val newFragment = HomeFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.content_frame, newFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}

