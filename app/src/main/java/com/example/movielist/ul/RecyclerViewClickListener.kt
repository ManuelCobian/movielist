package com.example.movielist.ul

interface RecyclerViewClickListener {
    fun onItemClick(position: String)
    fun onLongClick(position: Int)
}