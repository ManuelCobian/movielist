package com.example.movielist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.movielist.ul.activities.DashBoardActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getSupportActionBar()?.hide()
        val handler = Handler()
        handler.postDelayed(mSplashHandler,1000)

    }

    internal var mSplashHandler: Runnable = Runnable {
        // your code to do after handler completes
        openDetailsActivity()
    }
    private fun openDetailsActivity() {
        val intent = Intent(this, DashBoardActivity::class.java)
        startActivity(intent)
    }

}