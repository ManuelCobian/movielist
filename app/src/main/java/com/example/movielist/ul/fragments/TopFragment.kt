package com.example.movielist.ul.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movielist.R
import com.example.movielist.dl.MovieModel
import com.example.movielist.dl.MovieViewModelFactory
import com.example.movielist.ul.adapters.MoviesAdapter
import kotlinx.android.synthetic.main.fragment_home.*


class TopFragment : Fragment() {
    var recicler: RecyclerView?=null;
    private lateinit var viewModel: MovieModel
    val factory = MovieViewModelFactory()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel= ViewModelProviders.of(this,factory).get(MovieModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        var view:View;
        view=inflater.inflate(R.layout.fragment_home, container, false)
        recicler=view.findViewById(R.id.recyclerView);



        loadTerror()
        return view
    }

    private fun loadTerror() {
        viewModel.getTListMovie().observe(this, Observer{
            Log.i("data",it.toString())

            recyclerView.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(activity)
                adapter =MoviesAdapter(it)
            }

        })
    }


}