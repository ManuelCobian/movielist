package com.example.movielist.ul.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.movielist.R
import com.example.movielist.dl.MovieModel
import com.example.movielist.dl.MovieViewModelFactory

class DetailActivity : AppCompatActivity() {
    private lateinit var viewModel: MovieModel
    val factory = MovieViewModelFactory()
    var Id:Int=0;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        viewModel= ViewModelProviders.of(this,factory).get(MovieModel::class.java)
        val objetointen:Intent=intent
        val id=objetointen.getStringExtra("movie_id")
        //Toast.makeText(this,id,Toast.LENGTH_SHORT).show()
        var movie_title:TextView= findViewById<TextView>(R.id.text_nombre)
        var movie_date= findViewById<TextView>(R.id.text_date)
        var rating= findViewById<TextView>(R.id.text_raking)
        var overview= findViewById<TextView>(R.id.text_review)
        var photo=findViewById<ImageView>(R.id.imageView2)

        movie_title.setText(objetointen.getStringExtra("movie_title"))
        rating.setText(objetointen.getStringExtra("rating"))
        movie_date.setText(objetointen.getStringExtra("movie_date"))
        overview.setText(objetointen.getStringExtra("overview"))

        Glide.with(this).load("http://image.tmdb.org/t/p/w500${objetointen.getStringExtra("foto")}").into(photo)
    }
}