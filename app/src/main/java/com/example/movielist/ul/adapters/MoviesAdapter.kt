package com.example.movielist.ul.adapters

import android.content.Context
import android.content.Intent
import android.telecom.Call
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movielist.R
import com.example.movielist.app.App
import com.example.movielist.models.Response.ResultResponse
import com.example.movielist.ul.activities.DetailActivity
import kotlinx.android.synthetic.main.item.view.*

class MoviesAdapter(var movies: List<ResultResponse>): RecyclerView.Adapter<MoviesViewHolder>() {
    var onItemClick: ((ResultResponse) -> Unit)? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)
        return MoviesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        return holder.bind(movies[position])
    }
}

class MoviesViewHolder(itemView : View): RecyclerView.ViewHolder(itemView){
    private val photo: ImageView = itemView.findViewById(R.id.movieimage)
    private val title: TextView = itemView.findViewById(R.id.title_text)
    private val overview:TextView = itemView.findViewById(R.id.idioma_text)
    private val rating:TextView = itemView.findViewById(R.id.date_text)



    fun bind(movie: ResultResponse) {
        Glide.with(itemView.context).load("http://image.tmdb.org/t/p/w500${movie.backdrop_path}").into(photo)
        title.text = movie.title
        overview.text = movie.release_date
       rating.text = "Rating : "+movie.vote_average.toString()
        itemView.setOnClickListener{
            //Toast.makeText(App.instance,movie.id.toString(),Toast.LENGTH_SHORT).show()
            var intent:Intent= Intent(App.instance,DetailActivity::class.java)
            intent.putExtra("movie_title",movie.title.toString())
            intent.putExtra("movie_date",movie.release_date.toString())
            intent.putExtra("rating",movie.vote_average.toString())
            intent.putExtra("overview",movie.overview)
            intent.putExtra("foto",movie.backdrop_path)
            itemView.context.startActivity(intent)


        }
    }


}